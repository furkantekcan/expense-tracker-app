import 'expense.dart';
import 'package:flutter/material.dart';
import 'exepense_list_page.dart';



class ExpenseAdd extends StatefulWidget {
  @override
  _ExpenseAddState createState() => _ExpenseAddState();
}

class _ExpenseAddState extends State<ExpenseAdd> {
  final _formKey = GlobalKey<FormState>();
  TextEditingController descriptionController = TextEditingController();
  TextEditingController costController = TextEditingController();
  String category;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Add New Item'),
        backgroundColor: Colors.black54,
      ),
      body: Container(
        child: Padding(
            padding: const EdgeInsets.all(40.0),
            child: Form(
                key: _formKey,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    Padding(
                      padding: EdgeInsets.all(10.0),
                      child: TextFormField(
                        controller: descriptionController,
                        scrollPadding: EdgeInsets.all(30.0),
                        decoration: InputDecoration(
                            labelText: 'Description',
                            border: OutlineInputBorder(
                              borderRadius:
                                  BorderRadius.all(Radius.circular(10.0)),
                              gapPadding: 4,
                            )),
                        validator: (value) {
                          if (value.isEmpty) {
                            return 'Please enter some text';
                          }

                          return null;
                        },
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.all(10.0),
                      child: TextFormField(
                        controller: costController,
                        decoration: InputDecoration(
                            labelText: 'Cost',
                            border: OutlineInputBorder(
                              borderRadius:
                                  BorderRadius.all(Radius.circular(10.0)),
                              gapPadding: 4,
                            )),
                        validator: (value) {
                          if (value.isEmpty) {
                            return 'Please enter some text';
                          }

                          return null;
                        },
                      ),
                    ),
                    Padding(
                        padding: EdgeInsets.all(10.0),
                        child: Row(
                          children: <Widget>[
                            InputChip(
                                avatar: CircleAvatar(
                                  backgroundColor: Colors.grey.shade800,
                                  child: Icon(Icons.fastfood),
                                ),
                                label: Text('Food'),
                                onPressed: () {
                                  print('I am the one thing in life.');
                                  category = 'Food';
                                }),
                            InputChip(
                                avatar: CircleAvatar(
                                  backgroundColor: Colors.grey.shade800,
                                  child: Icon(Icons.fastfood),
                                ),
                                label: Text('A0'),
                                onPressed: () {
                                  print('I am the one thing in life.');
                                  category = 'A0';
                                }),
                            InputChip(
                                avatar: CircleAvatar(
                                  backgroundColor: Colors.grey.shade800,
                                  child: Icon(Icons.fastfood),
                                ),
                                label: Text('A1'),
                                onPressed: () {
                                  print('I am the one thing in life.');
                                  category = 'A1';
                                }),
                            InputChip(
                                avatar: CircleAvatar(
                                  backgroundColor: Colors.grey.shade800,
                                  child: Icon(Icons.fastfood),
                                ),
                                label: Text('A2'),
                                onPressed: () {
                                  print('I am the one thing in life.');
                                  category = 'A2';
                                })
                          ],
                        )),
                    Padding(
                      padding:
                          EdgeInsets.symmetric(vertical: 8.0, horizontal: 32.0),
                      child: RaisedButton(
                        color: Colors.black54,
                        textColor: Colors.white,
                        onPressed: () {
                          submitItem(context);
                        },
                        child: Text('Submit'),
                      ),
                    )
                  ],
                ))),
      ),
    );
  }

  void submitItem(BuildContext context) {
    if (_formKey.currentState.validate()) {
      Scaffold.of(context).showSnackBar(SnackBar(content: Text('Processing data')));
      
      var  newExpense = Expense(category, descriptionController.text, costController.text);
      expenseList.add(newExpense);
    }
  }
}

class Expense {
  final String category;
  final String description;
  final String cost;

  Expense(this.category, this.description, this.cost);
}
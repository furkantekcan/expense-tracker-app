import 'package:flutter/material.dart';
import 'category_add_page.dart';

class Category extends StatefulWidget {
  @override
  _CategoryState createState() => _CategoryState();
}

class _CategoryState extends State<Category> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Category'),
        backgroundColor: Colors.black54,
      ),
    
    body: Center(
      child:  ListView(
          children: <Widget>[
            ListTile(
              leading: Icon(Icons.fastfood),
              title: Text('Food'),
              trailing: Text(10.toString()+'\$'),
            ),
           ListTile(
              leading: Icon(Icons.shopping_cart),
              title: Text('Clothes'),
              trailing: Text(100.toString()+'\$'),
            ),
            
          ],
        ),
    ),
    floatingActionButton: FloatingActionButton.extended(
      onPressed: () {
        // Add your onPressed code here!
        Navigator.of(context).push(
    MaterialPageRoute(
      builder: (BuildContext context) {
        return CategoryAdd();
      },
    ),
  );
      },
      label: Text('New Category'),
      icon: Icon(Icons.add),
      backgroundColor: Colors.pink,
    ),
  );
  }
}

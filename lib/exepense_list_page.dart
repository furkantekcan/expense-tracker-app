import 'package:flutter/material.dart';
import 'expense.dart';

class ExpenseList extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: const Text('Expenses'),
          backgroundColor: Colors.black54,
        ),
        body: ListView.builder(
          // Must have an item count equal to the number of items!
          itemCount: expenseList.length,
          // A callback that will return a widget.
          itemBuilder: (context, int) {
            // In our case, a DogCard for each doggo.
            return ListTile(
              title: Text(expenseList[int].description),
              leading: Text(expenseList[int].category),
              trailing: Text(expenseList[int].cost),
            );
          },
        ));
  }
}

var expenseList = <Expense> [];
